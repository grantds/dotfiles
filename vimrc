set backspace=2
syntax on
filetype indent on 
set autoindent
set nobackup
set expandtab
set shiftwidth=4
set tabstop=4
set background=dark
set hlsearch
set t_Co=256
colorscheme diablo3

" for command mode
nmap <S-Tab> <<
" for insert mode
imap <S-Tab> <Esc><<1

